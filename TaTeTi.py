import numpy as np
import pickle

BOARD_ROWS = 3
BOARD_COLS = 3


class State:
    def __init__(self, p1, p2):
        self.board = np.zeros((BOARD_ROWS, BOARD_COLS))
        self.p1 = p1
        self.p2 = p2
        self.finalizado = False
        self.boardHash = None
        # jugador 1 empieza
        self.jugadorNumero = 1

    #hash unico del tablero
    def getHash(self):
        self.boardHash = str(self.board.reshape(BOARD_COLS * BOARD_ROWS))
        return self.boardHash

    def ganador(self):
        # fila
        for i in range(BOARD_ROWS):
            if sum(self.board[i, :]) == 3:
                self.finalizado = True
                return 1
            if sum(self.board[i, :]) == -3:
                self.finalizado = True
                return -1
        # col
        for i in range(BOARD_COLS):
            if sum(self.board[:, i]) == 3:
                self.finalizado = True
                return 1
            if sum(self.board[:, i]) == -3:
                self.finalizado = True
                return -1
        # diagonal
        diag_sum1 = sum([self.board[i, i] for i in range(BOARD_COLS)])
        diag_sum2 = sum([self.board[i, BOARD_COLS - i - 1] for i in range(BOARD_COLS)])
        diag_sum = max(abs(diag_sum1), abs(diag_sum2))
        if diag_sum == 3:
            self.finalizado = True
            if diag_sum1 == 3 or diag_sum2 == 3:
                return 1
            else:
                return -1

        # empate
        # no hay movimiento posibles
        if len(self.posicionesDisponibles()) == 0:
            self.finalizado = True
            return 0
        self.finalizado = False
        return None

    def posicionesDisponibles(self):
        posiciones = []
        for i in range(BOARD_ROWS):
            for j in range(BOARD_COLS):
                if self.board[i, j] == 0:
                    posiciones.append((i, j))  # need to be tuple
        return posiciones

    def actualizarEstado(self, posicion):
        self.board[posicion] = self.jugadorNumero
        #cambiar de jugador
        self.jugadorNumero = -1 if self.jugadorNumero == 1 else 1

    #solo cuando termina el juego
    def darrecompensa(self):
        resultado = self.ganador()
        # retropropagar recompensa
        if resultado == 1:
            self.p1.alimentarRecompensa(1)
            self.p2.alimentarRecompensa(0)
        elif resultado == -1:
            self.p1.alimentarRecompensa(0)
            self.p2.alimentarRecompensa(1)
        else:
            self.p1.alimentarRecompensa(0.1)
            self.p2.alimentarRecompensa(0.5)

    #resetear tablero
    def reset(self):
        self.board = np.zeros((BOARD_ROWS, BOARD_COLS))
        self.boardHash = None
        self.finalizado = False
        self.jugadorNumero = 1

    def play(self, Rondas=100):
        for i in range(Rondas):
            if i % 1000 == 0:
                print("Rondas {}".format(i))
            while not self.finalizado:
                # jugador 1
                posiciones = self.posicionesDisponibles()
                p1_action = self.p1.elegirAccion(posiciones, self.board, self.jugadorNumero)
                #tomar accion y actualizar estado
                self.actualizarEstado(p1_action)
                board_hash = self.getHash()
                self.p1.añadirEstado(board_hash)
                # chequea si el estado es finalizado

                ganar = self.ganador()
                if ganar is not None:
                    self.mostrarTablero()
                    # terminado con p1 ganador o empate
                    self.darrecompensa()
                    self.p1.reset()
                    self.p2.reset()
                    self.reset()
                    break

                else:
                    # jugador 2
                    posiciones = self.posicionesDisponibles()
                    p2_action = self.p2.elegirAccion(posiciones, self.board, self.jugadorNumero)
                    self.actualizarEstado(p2_action)
                    board_hash = self.getHash()
                    self.p2.añadirEstado(board_hash)

                    ganar = self.ganador()
                    if ganar is not None:
                        # self.mostrarTablero()
                        
                        self.darrecompensa()
                        self.p1.reset()
                        self.p2.reset()
                        self.reset()
                        break

    #jugar con humano
    def play2(self):
        while not self.finalizado:
            # jugador 1
            posiciones = self.posicionesDisponibles()
            p1_action = self.p1.elegirAccion(posiciones, self.board, self.jugadorNumero)
            #tomar accion y actualizar estado
            self.actualizarEstado(p1_action)
            self.mostrarTablero()
            # chequea si el estado es finalizado

            ganar = self.ganador()
            if ganar is not None:
                if ganar == 1:
                    print(self.p1.name, "Gana!")
                else:
                    print("Empate!")
                self.reset()
                break

            else:
                # jugador 2
                posiciones = self.posicionesDisponibles()
                p2_action = self.p2.elegirAccion(posiciones)

                self.actualizarEstado(p2_action)
                self.mostrarTablero()
                ganar = self.ganador()
                if ganar is not None:
                    if ganar == -1:
                        print(self.p2.name, "Gana!")
                    else:
                        print("Empate!")
                    self.reset()
                    break

    def mostrarTablero(self):
        # p1: x  p2: o
        for i in range(0, BOARD_ROWS):
            print('-------------')
            out = '| '
            for j in range(0, BOARD_COLS):
                if self.board[i, j] == 1:
                    token = 'x'
                if self.board[i, j] == -1:
                    token = 'o'
                if self.board[i, j] == 0:
                    token = ' '
                out += token + ' | '
            print(out)
        print('-------------')


class jugador:
    def __init__(self, name, exp_rate=0.3):
        self.name = name
        self.estados = []  # graba los estados
        self.lr = 0.2
        self.exp_rate = exp_rate
        self.decay_gamma = 0.9
        self.estados_value = {}  # state -> value

    def getHash(self, board):
        boardHash = str(board.reshape(BOARD_COLS * BOARD_ROWS))
        return boardHash

    def elegirAccion(self, posiciones, tablero_actual, symbol):
        if np.random.uniform(0, 1) <= self.exp_rate:
            # tomar accion aleatoria
            idx = np.random.choice(len(posiciones))
            action = posiciones[idx]
        else:
            value_max = -999
            for p in posiciones:
                next_board = tablero_actual.copy()
                next_board[p] = symbol
                next_boardHash = self.getHash(next_board)
                value = 0 if self.estados_value.get(next_boardHash) is None else self.estados_value.get(next_boardHash)
                if value >= value_max:
                    value_max = value
                    action = p
        return action

    # añadir un estado hash
    def añadirEstado(self, state):
        self.estados.append(state)

    def alimentarRecompensa(self, recompensa):
        for st in reversed(self.estados):
            if self.estados_value.get(st) is None:
                self.estados_value[st] = 0
            self.estados_value[st] += self.lr * (self.decay_gamma * recompensa - self.estados_value[st])
            recompensa = self.estados_value[st]

    def reset(self):
        self.estados = []

    def salvarPolitica(self):
        fw = open('politica_' + str(self.name), 'wb')
        pickle.dump(self.estados_value, fw)
        fw.close()

    def loadpolitica(self, file):
        fr = open(file, 'rb')
        self.estados_value = pickle.load(fr)
        fr.close()


class jugadorHumano:
    def __init__(self, name):
        self.name = name

    def elegirAccion(self, posiciones):
        while True:
            row = int(input("Ingresar accion en fila: "))
            col = int(input("Ingresar accion en columna: "))
            action = (row, col)
            if action in posiciones:
                return action
            
    def añadirEstado(self, state):
        pass

    def alimentarRecompensa(self, recompensa):
        pass

    def reset(self):
        pass
    

if __name__ == "__main__":
    # entrenando
    p1 = jugador("p1")
    p2 = jugador("p2")

    st = State(p1, p2)
    print("entrenando...")
    st.play(5000)



    # play with human
    p1 = jugador("computadora", exp_rate=0)
    p1.loadpolitica("politica_p1")

    p2 = jugadorHumano("humano")

    st = State(p1, p2)
    st.play2()